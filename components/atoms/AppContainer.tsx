
import styled from 'styled-components'

const AppContainer = styled.div`
  display: block;
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  align-self: stretch;
  flex: 0 auto;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: auto auto;
  overflow: hidden;
`

export default AppContainer