
import styled from 'styled-components'

const Title = styled.h2`
  font-family: 'Futura', sans-serif;
  font-size: 22px;
  margin-bottom: 15px;
  font-weight: 400;
  text-align: left;

  @media screen and (max-width: 479px) {
    text-align: center;
  }
`


export default Title