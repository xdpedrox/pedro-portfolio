import Container from "./Container";
import IconContainer from "./IconContainer";
import Icon from "./Icon";
import Title from "./Title";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  Container,
  IconContainer,
  Icon,
  Title,
}