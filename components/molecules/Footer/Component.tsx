import { Container } from "./Container";
import { FooterBox } from "./FooterBox";
import {P} from "../../atoms/P";
import * as Heading from "../../atoms/Heading";

export function Footer() {
  return (
    <Container>
      <FooterBox>
        <P white>
          Designed and Created <br />
          by Pedro Roque
        </P>
      </FooterBox>
    </Container>
  );
}
