export { ColoredSection, WhiteSection } from "./Section";
export { ImageBox } from "./contents/ImageBox";
export { ButtonsBox } from "./contents/ButtonsBox";
export { InfoBox } from "./contents/InfoBox";
export { HeadingBox } from "./contents/HeadingBox";
export { Divider } from "./contents/Divider";

// Container
export { BaseContainer } from "./containers/BaseContainer";
export { EducationContainer } from "./containers/EducationContainer";
export { ProjectContainer } from "./containers/ProjectContainer";
export { BasicContainer } from "./containers/BasicContainer";
