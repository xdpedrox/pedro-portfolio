
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  height: 35px;
  margin: 0px;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-left: 7px;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  border-radius: 11px;
  background-color: #fff;
`

export default Container