import Container from "./Container";
import InBar from "./InBar";
import OutBar from "./OutBar";
import Title from "./Title";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  Container,
  InBar,
  OutBar,
  Title,
}