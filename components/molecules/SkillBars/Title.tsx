
import styled from 'styled-components'

const Title = styled.div`
  margin-left: 0px;
  color: #fff;
  margin-top: 20px;
  margin-bottom: 10px;
  font-size: 25px;
`

export default Title