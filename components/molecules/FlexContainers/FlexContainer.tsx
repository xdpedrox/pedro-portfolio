import styled from 'styled-components'

export const FlexContainer = styled.div`
  margin-top: 20px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: transparent;
  flex-wrap: wrap;
  justify-content: space-between;
  `
