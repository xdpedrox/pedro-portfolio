import styled from 'styled-components'

export const FlexItem = styled.span`
    max-width: 500px;
    min-width: 450px;
    margin: 20px;
  `
